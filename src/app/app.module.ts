import { BaseService } from './../_shared/_services/base.service';
import { ApiService } from './../_shared/_services/api.service';
import { BaseApiService } from './../_shared/_classes/base-api.service';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { StarRatingModule } from 'ionic3-star-rating';
import { EnquiryModule } from "../components/enquiry/enquiry.module";
import { ReviewModule } from "../components/review/review.module";
import { Shared } from '../_shared/_classes/share.class';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    MyApp,
    ListPage
  ],
  imports: [
    BrowserModule,
    // StarRatingModule,
    EnquiryModule,
    ReviewModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BaseApiService,
    ApiService,
    BaseService,
    Shared,
  ]
})
export class AppModule {}
