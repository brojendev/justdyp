import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { PrimaryHeadersModule } from '../../components/primary-header/primary-header.module';
import { PrimaryFooterModule } from '../../components/primary-footer/primary-footer.module';
import { DirectoryModule } from '../../components/directory/directory.module';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    PrimaryHeadersModule,
    PrimaryFooterModule,
    DirectoryModule,
    PipesModule
  ],
})
export class HomePageModule {}
