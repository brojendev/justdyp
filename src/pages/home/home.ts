import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { BaseComponent } from '../../_shared/_classes/base.component';
import { homeCategoryResponse, homeDirectoryResponse } from '../../_shared/constant';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage extends BaseComponent {
  public loader = this.base.shared.Loader.present_loader();

  public categories: any;
  public interationTimes = [];
  public featuredDirectories: any = [];
  public mediaBaseUrl: String;
  public directoryPageNo: number = 1;
  public moreDirectoryData: number = 1;
  public infiniteScroll: any;
  public homePageBanner: any = [];
  ionViewWillEnter(){
    this.loader.present();
    this.base.api.getHomeCategories();
    this.base.api.getHomeDirectories({pageno: this.directoryPageNo});
    this.loader.dismiss();
  }

  handleApiResponse(data){
    if(data.resultType === homeCategoryResponse){
      this.populateHomeCategoryList(data);
    }
    if(data.resultType === homeDirectoryResponse){
      this.populateDirectoryList(data);
    }
  }

  populateHomeCategoryList(data){
    this.categories = data.result.home_categories;
    this.homePageBanner = data.result.home_banner;
    this.interationTimes = [];
    const currLength = (this.categories.length % 3 === 0) ? this.categories.length / 3 : ((this.categories.length / 3) + 1);
    for(let i = 0; i < currLength; i++)
      this.interationTimes.push(i);
  }

  populateDirectoryList(data){
    if(data.result.response_data.directories !== undefined && this.featuredDirectories !== undefined ){
      this.featuredDirectories = this.featuredDirectories.concat(data.result.response_data.directories);
      this.mediaBaseUrl = data.result.response_data.media_base_url;
      if(this.directoryPageNo > 1){
        this.infiniteScroll.complete();
      }
      this.directoryPageNo ++;
    } else {
      this.moreDirectoryData = 0
    }
    
    // console.log(this.mediaBaseUrl);
  }

  loadMoreFeaturDirectory(event){
    this.infiniteScroll = event;
    this.base.api.getHomeDirectories({pageno: this.directoryPageNo});
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.navCtrl.setRoot(page);
  }

  pushPage(page, data: any ={}) {
    console.log('Data', data);
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.navCtrl.push(page, {data: data});
  }

}
