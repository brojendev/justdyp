import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DirectoryListPage } from './directory-list';
import { PrimaryHeadersModule } from '../../components/primary-header/primary-header.module';
import { PrimaryFooterModule } from '../../components/primary-footer/primary-footer.module';
import { DirectoryModule } from '../../components/directory/directory.module';

@NgModule({
  declarations: [
    DirectoryListPage,
  ],
  imports: [
    IonicPageModule.forChild(DirectoryListPage),
    PrimaryHeadersModule,
    PrimaryFooterModule,
    DirectoryModule
  ],
})
export class DirectoryListPageModule {}
