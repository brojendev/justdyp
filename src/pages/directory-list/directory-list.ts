import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BaseComponent } from '../../_shared/_classes/base.component';
import { directoryResponse } from '../../_shared/constant';
/**
 * Generated class for the DirectoryListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-directory-list',
  templateUrl: 'directory-list.html',
})
export class DirectoryListPage extends BaseComponent {

  public loader = this.base.shared.Loader.present_loader();
  public directories: any = [];
  public pageNo: number = 1;
  public mediaBaseUrl: String;
  public moreDirectoryData: number = 1;
  public infiniteScroll: any;
  category: any;

  ionViewWillEnter(){
    this.category = this.navParams.get('category');
    if(this.category !== undefined){
      var query = {category_id: this.category._id, pageno: this.pageNo};
    } else {
      this.navCtrl.setRoot('CategoryListPage');
    }
    
    console.log(query);
    this.loader.present();
    this.base.api.getDirectoriesByCategory(query);
    this.loader.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DirectoryListPage');
  }

  handleApiResponse(data){
    if(data.resultType === directoryResponse){
      this.populateDirectoryList(data);
    }
  }

  populateDirectoryList(data){
    if(data.result.response_data.directories !== undefined ){
      this.directories = this.directories.concat(data.result.response_data.directories);
      this.mediaBaseUrl = data.result.response_data.media_base_url;
      this.pageNo ++;
    } else {
      this.moreDirectoryData = 0;
    }
  }

  loadMoreDirectory(event){
    this.infiniteScroll = event;
    if(this.category !== undefined){
      var query = {category_id: this.category._id, pageno: this.pageNo};
      this.base.api.getDirectoriesByCategory(query);
    } else {
      this.navCtrl.setRoot('CategoryListPage');
    }
  }

}
