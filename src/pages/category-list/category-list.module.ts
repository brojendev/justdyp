import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoryListPage } from './category-list';
import { PrimaryHeadersModule } from '../../components/primary-header/primary-header.module';
import { PrimaryFooterModule } from '../../components/primary-footer/primary-footer.module';

@NgModule({
  declarations: [
    CategoryListPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoryListPage),
    PrimaryHeadersModule,
    PrimaryFooterModule
  ],
})
export class CategoryListPageModule {}
