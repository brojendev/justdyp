import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BaseComponent } from '../../_shared/_classes/base.component';
import { categoryResponse } from '../../_shared/constant';

/**
 * Generated class for the CategoryListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-category-list',
  templateUrl: 'category-list.html',
})
export class CategoryListPage extends BaseComponent {
  public loader = this.base.shared.Loader.present_loader();
  public categories: any = [];
  public pageNo: number = 1;
  public moreCategoryData: number = 1;
  public infiniteScroll: any;
  public category: any;
  public searchKeyword: string;
  public query: any = {};
  ionViewWillEnter(){
    // this.categories = [];
    this.category = this.navParams.get('data');
    this.searchKeyword = this.navParams.get('searchKeyword');

    
    this.loader.present();
    this.getCategories();
    this.loader.dismiss();
  }

  getCategories(){
    if(this.category !== undefined && this.category._id !== undefined){
      this.query.category_id = this.category._id;
    }
    if(this.searchKeyword !== undefined){
      this.query.search_keyword = this.searchKeyword;
    }
    this.query.pageno = this.pageNo;
    console.log('Query', this.query);
    this.base.api.getAllCategories(this.query);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryListPage');
  }

  handleApiResponse(data){
    if(data.resultType === categoryResponse){
      this.populateCategoryList(data);
    }
  }

  populateCategoryList(data){
    console.log('Data', data);
    if(data.result.response_data.categories !== undefined ){
      if(data.result.response_data.categories.length < 1){
        this.moreCategoryData = 0;
        return;
      }
      if(this.categories !== undefined){
        this.categories = this.categories.concat(data.result.response_data.categories);
      }
      
      if(this.infiniteScroll != undefined && this.pageNo > 1){
        this.infiniteScroll.complete();
      }
      this.pageNo ++;
    } else { 
      this.moreCategoryData = 0;
    }
    
    console.log('category', this.categories);
  }

  loadMoreCategory(event){
    this.infiniteScroll = event;
    this.getCategories();
  }

  goToDirectoryList(item: any){
    this.navCtrl.push('DirectoryListPage', {category: item})
  }

}
