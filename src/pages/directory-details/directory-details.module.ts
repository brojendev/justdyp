import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DirectoryDetailsPage } from './directory-details';
import { PrimaryHeadersModule } from '../../components/primary-header/primary-header.module';
import { PrimaryFooterModule } from '../../components/primary-footer/primary-footer.module';
import { DirectoryModule } from '../../components/directory/directory.module';

@NgModule({
  declarations: [
    DirectoryDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(DirectoryDetailsPage),
    PrimaryHeadersModule,
    PrimaryFooterModule,
    DirectoryModule
  ],
})
export class DirectoryDetailsPageModule {}
