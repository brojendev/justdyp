import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BaseComponent } from '../../_shared/_classes/base.component';
/**
 * Generated class for the DirectoryDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-directory-details',
  templateUrl: 'directory-details.html',
})
export class DirectoryDetailsPage extends BaseComponent {
  directory: any;
  base_url: String;
  ionViewWillEnter(){
    this.directory = this.navParams.get('directory');
    this.base_url = this.navParams.get('mediaBaseUrl');
    console.log('this.directory', this.directory)
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DirectoryDetailsPage');
  }

}
