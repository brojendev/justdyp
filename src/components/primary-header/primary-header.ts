import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the PrimaryHeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'primary-header',
  templateUrl: 'primary-header.html'
})
export class PrimaryHeaderComponent {

  text: string;
  searchText: string;
  constructor(
    public navCtrl: NavController
  ) {
    console.log('Hello PrimaryHeaderComponent Component');
    this.text = 'Hello World';
  }

  genericSearch(){
    this.navCtrl.push('CategoryListPage', {searchKeyword: this.searchText});
  }

}
