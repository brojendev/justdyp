import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { PrimaryHeaderComponent } from './primary-header';
@NgModule({
	declarations: [PrimaryHeaderComponent],
	imports: [
		IonicModule
	],
	exports: [
		PrimaryHeaderComponent
	]
})
export class PrimaryHeadersModule {}
