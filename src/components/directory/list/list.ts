import { Component, Input, Output, OnInit } from '@angular/core';
import { ModalController, NavParams, NavController } from 'ionic-angular';
import { EnquiryComponent } from "../../enquiry/enquiry";
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

/**
 * Generated class for the DirectoryComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'list-directory',
  templateUrl: 'list.html'
})
export class ListComponent {

  text: string;
  directory_data: any;
  base_url: String;
  sanitizedContent: SafeResourceUrl;
  @Input('directoryData') 
  set _directoryData(data: any) {
    this.directory_data = data;
 }
 @Input('mediaBaseUrl') 
  set _mediaBaseUrl(data: any) {
    this.base_url = data;
    console.log(this.base_url);
 }
  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public sanitizer: DomSanitizer,
  ) {
    this.text = 'Hello list';
  }

  directoryPrimaryImage(media_details){
    var image = media_details.find(function(media){
      return media.group_name == "image";
    });

    if(image !== undefined){
      var dir_image = this.base_url+image.media_content;
      var sanitizedContent = this.sanitizer.bypassSecurityTrustUrl(dir_image);
      // return sanitizedContent;
      return dir_image;
    }
  }
  ionViewWillEnter(){
    console.log('directory_data', this.directory_data);
  }

  openDetails(){
    this.navCtrl.push('DirectoryDetailsPage',{directory: this.directory_data, mediaBaseUrl: this.base_url});
  }

  openEnquiry(){
    let profileModal = this.modalCtrl.create(EnquiryComponent, { directory: this.directory_data });
    profileModal.present();
  }

  callMobile(){
    let phones = this.directory_data.contact_info.find(function(data){
      return data.group_name == 'mobile';
    });
    console.log('phones', phones);
    if(phones)
      window.open("tel:"+phones.information_data);  
  }

  ngOnInit() {
    var image = this.directory_data.media_details.find(function(media){
      return media.group_name == "image";
    });

    if(image !== undefined){
      var dir_image = this.base_url+image.media_content;
      this.sanitizedContent = this.sanitizer.bypassSecurityTrustUrl(dir_image);
      // return sanitizedContent;
    }
  }

}
