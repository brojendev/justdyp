import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ListComponent } from './list/list';
import { DetailsComponent } from './details/details';
import { StarRatingModule } from "ionic3-star-rating";
import { EnquiryModule } from "../enquiry/enquiry.module";
import { ReviewModule } from "../review/review.module";

@NgModule({
	declarations: [
        ListComponent,
        DetailsComponent
    ],
	imports: [
        IonicModule,
        EnquiryModule,
        ReviewModule,
        StarRatingModule
    ],
	exports: [
        ListComponent,
        DetailsComponent
    ]
})
export class DirectoryModule {}
