import { Component, Input } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { EnquiryComponent } from "../../enquiry/enquiry";
import { ReviewComponent } from "../../review/review";

/**
 * Generated class for the DirectoryComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'directory-details',
  templateUrl: 'details.html'
})
export class DetailsComponent {

  text: string;
  directory_data: any;
  emails: any;
  phones: any;
  web: any;
  base_url: String;
  avg_rating: number = 0;
  rating: number = 0;
  @Input('directoryData') 
  set _directoryData(data: any) {
    this.directory_data = data;
    console.log(this.directory_data);
    if(this.directory_data !== undefined){
      this.phones = this.directory_data.contact_info.find(function(data){
        return data.group_name == 'mobile';
      });

      this.emails = this.directory_data.contact_info.find(function(data){
        return data.group_name == 'email';
      });

      this.web = this.directory_data.contact_info.find(function(data){
        return data.group_name == 'weburl';
      });
      let self = this;
      this.directory_data.rating_review_details.map(function(data){
        self.avg_rating += parseInt(data.rating);
      });

      this.avg_rating = this.avg_rating/this.directory_data.rating_review_details.length
      console.log();

    }
    
  }
  @Input('mediaBaseUrl') 
  set _mediaBaseUrl(data: any) {
    this.base_url = data;
    console.log(this.base_url);
  }

  constructor(
    public modalCtrl: ModalController,
    public events: Events
  ) {
    console.log('Hello DirectoryComponent Component');
    this.text = 'SKIPPER FURNISHINGS';
    this.directory_data = {};
    this.base_url = '';
    let self = this;
    events.subscribe('star-rating:changed', (starRating) => {
      self.rating = starRating;
    });
  }

  findRatingPercent(rating){
    return (rating*100)/5;
  }
  findEmail(){
    
    if(this.emails)
      return this.emails.information_data;
  }

  findPhone(){
    
    if(this.phones)
      return this.phones.information_data;
  }

  findWeb(){
    
    if(this.web)
      return this.web.information_data;
  }

  openEnquiry(){
    let enquiryModal = this.modalCtrl.create(EnquiryComponent, { directory: this.directory_data });
    enquiryModal.present();
  }
  openReviewForm(){
    let reviewModal = this.modalCtrl.create(ReviewComponent, { directory: this.directory_data, rating: this.rating });
    reviewModal.present();
  }
  openLink(){
    if(this.web)
      window.open("//"+this.web.information_data);    
  }

  callMobile(){
    if(this.phones)
      window.open("tel:"+this.phones.information_data);  
  }

}
