import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PrimaryFooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'primary-footer',
  templateUrl: 'primary-footer.html'
})
export class PrimaryFooterComponent {

  text: string;
  current_page: string;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams ) {

    // console.log('Hello PrimaryFooterComponent Component', this.navCtrl);
    this.text = 'Hello World';
    // this.current_page = this.navCtrl.getActive().name;
    // if(this.navCtrl.getActive() !== undefined)
    //   console.log('Page :', this.navCtrl.getActive().name);
  }

  ionViewWillEnter() {
    var current = this.navCtrl.getActive();
    console.log('Page Object :', current  );
    if(current !== undefined){
      console.log('Page :', current.name);
    }
  }

  openPage(page) {
    
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.current_page = this.navCtrl.getActive().name;
    if( this.current_page!= page)
      this.navCtrl.setRoot(page);
  }

  pushPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.current_page = this.navCtrl.getActive().name;
    if(this.current_page != page)
      this.navCtrl.push(page);
  }

}
