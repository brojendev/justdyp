import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { PrimaryFooterComponent } from './primary-footer';
@NgModule({
	declarations: [PrimaryFooterComponent],
	imports: [
		IonicModule	
	],
	exports: [
		PrimaryFooterComponent
	]
})
export class PrimaryFooterModule {}
