import { NgModule } from '@angular/core';
import { ReviewComponent } from './review';
import { IonicModule } from 'ionic-angular';
import { StarRatingModule } from "ionic3-star-rating";
@NgModule({
	declarations: [ReviewComponent],
	imports: [
		IonicModule,
		StarRatingModule
	],
	exports: [ReviewComponent],
	entryComponents: [
        ReviewComponent 
    ],
})
export class ReviewModule {}
