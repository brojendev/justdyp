import { NgModule } from '@angular/core';
import { EnquiryComponent } from './enquiry';
import { IonicModule } from 'ionic-angular';
import { StarRatingModule } from "ionic3-star-rating";
@NgModule({
	declarations: [EnquiryComponent],
	imports: [
		IonicModule,
		StarRatingModule
	],
	exports: [EnquiryComponent],
	entryComponents: [
        EnquiryComponent 
    ],
})
export class EnquiryModule {}
