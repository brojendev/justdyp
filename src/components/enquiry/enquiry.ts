import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BaseComponent } from '../../_shared/_classes/base.component';
import { enquiryResponse } from '../../_shared/constant';
/**
 * Generated class for the EnquiryComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'enquiry',
  templateUrl: 'enquiry.html'
})
export class EnquiryComponent extends BaseComponent{

  text: string;
  enquiry: any = {};
  directory: any;
  // constructor(
  //   navparam: NavParams,
  //   shared: Shared
  // ) {
  //   super();
  //   console.log('Hello EnquiryComponent Component');
  //   this.text = 'Hello World';
  //   this.enquiry = {};

  //   console.log(navparam.get('directoryID'))
  // }
  
  ionViewWillEnter(){
    this.directory = this.navParams.get('directory');
    this.text = this.directory.business_name; 
    this.enquiry = {};
  }
  submitEnquiryForm(){
    console.log(this.enquiry);
    var mob = /^[1-9]{1}[0-9]{9}$/;
    if(this.enquiry.name == undefined || this.enquiry.name.trim() == ''){
      this.base.shared.Alert.show_alert('Error!', 'Please enter name');
      return;
    }
    if(this.enquiry.mobile == undefined || this.enquiry.mobile.trim() == ''){
      this.base.shared.Alert.show_alert('Error!', 'Please enter mobile');
      return;
    } else if(mob.test(this.enquiry.mobile) == false){
      this.base.shared.Alert.show_alert('Error!', 'Please enter valid mobile');
      return; 
    }

    if(this.enquiry.details == undefined || this.enquiry.details.trim() == ''){
      this.base.shared.Alert.show_alert('Error!', 'Please enter enquiry description');
      return;
    }
    this.enquiry.directory_id = this.directory.id;

    this.base.api.submitDirectoryEnquiry(this.enquiry);

  }

  handleApiResponse(data){
    if(data.resultType === enquiryResponse){
      this.showResponse(data);
    }
  }

  showResponse(response){
    this.base.shared.Alert.show_alert('Success', response.result.message);
    this.viewCtrl.dismiss();
  }

  closeModal(){
    this.viewCtrl.dismiss(); 
  }

}
