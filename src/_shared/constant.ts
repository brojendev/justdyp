export const API_URL  = 'http://api.justdyp.com/api/v1/';

export const requestGet = 'getRequest';
export const requestPost = 'postRequest';
export const requestPut = 'requestPut';
export const requestDelete = 'requestDelete';
export const errorResult = 'errorResponse';

export const homeCategoryResponse = 'homeCategoryResponse';
export const homeDirectoryResponse = 'homeDirectoryResponse';
export const categoryResponse = 'categoryResponse';
export const directoryResponse = 'directoryResponse';
export const enquiryResponse = 'enquiryResponse';
