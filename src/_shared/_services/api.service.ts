import { BaseApiService } from './../_classes/base-api.service';
import { Injectable } from '@angular/core';
import * as con from './../constant';


@Injectable()
export class ApiService extends BaseApiService {

  public HOME_CATEGORY_URL = con.API_URL + 'category/home-category';
  public HOME_DIRECTORY_URL = con.API_URL + 'directory/home-directory';
  public CATEGORY_URL = con.API_URL + 'category';
  public DIRECTORY_BY_CATEGORY_URL = con.API_URL + 'directory';
  public DIRECTORY_ENQUIRY_URL = con.API_URL + 'directory/enquiry';

  public getHomeCategories(params: any = {}){
    this.genericApiCall(this.HOME_CATEGORY_URL, con.homeCategoryResponse, params, con.requestPost, true);
  }

  public getHomeDirectories(params: any = {}){
    this.genericApiCall(this.HOME_DIRECTORY_URL, con.homeDirectoryResponse, params, con.requestPost, true);
  }

  public getAllCategories(params: any = {}){
    this.genericApiCall(this.CATEGORY_URL, con.categoryResponse, params, con.requestPost, true);
  }

  public getDirectoriesByCategory(params: any = {}){
    this.genericApiCall(this.DIRECTORY_BY_CATEGORY_URL, con.directoryResponse, params, con.requestPost, true);
  }

  public submitDirectoryEnquiry(params: any = {}){
    this.genericApiCall(this.DIRECTORY_ENQUIRY_URL, con.enquiryResponse, params, con.requestPost, true);
  }
}
