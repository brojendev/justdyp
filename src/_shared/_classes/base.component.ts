import { NavParams, NavController } from 'ionic-angular';
import { BaseService } from './../_services/base.service';
import { errorResult } from './../constant';
import { Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ViewController } from 'ionic-angular';

@Injectable()
export class BaseComponent implements OnDestroy {

  private apiSubscription: Subscription;

  /**
   * manage dependency injcection of services
   *
   * @param baseService base service
   */
  constructor(
    public base: BaseService,
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {
    this.apiSubscription = this.base.api.apiResults.subscribe(data => {
      if(data.length != 0){
        if (data.resultType === errorResult) {
          const errorMessage = data.result.message ? data.result.message : 'something went wrong';
          this.base.shared.Alert.show_alert('Error!', errorMessage);
          return true;
        }
        else if( data.result === undefined || data.result === null || (data.result !== undefined && data.result.status != 1) ){
          const errorMessage = data.result && data.result.message ? data.result.message : 'something went wrong';
          this.base.shared.Alert.show_alert('Error!', errorMessage);
          return true;
        } else {
          this.handleApiResponse(data);
        }
      }
    });
  }

  /**
   * this will handel all api response, must be override on child component
   *
   * @param data any
   */
  handleApiResponse(data: any) { }

  handleValidationError(data: any) { }

  /**
   * unsubscribe api response subscriber on component destroy
   */
  ngOnDestroy() {
    this.apiSubscription.unsubscribe();
  }
}
