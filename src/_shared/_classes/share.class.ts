import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';

@Injectable()
export class Shared {
  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ){}

  public Alert = {
    show_alert: (title: string = 'Alert', subTitle: string = '') => {
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: subTitle,
        buttons: ['OK']
      });
      return alert.present();
    },
    present_confirm: (title: string = 'Alert', subTitle: string = '', trueCallback =  Function , falseCallback = Function) => {
      let alert = this.alertCtrl.create({
        title: title,
        message: subTitle,
        buttons: [
          {
            text: 'NO',
            role: 'cancel',
            handler: () => {
              trueCallback();
            }
          },
          {
            text: 'YES',
            handler: () => {
              falseCallback();
            }
          }
        ]
      });
      return alert.present();
    }
  };

  public loading;

  public Loader = {
    present_loader: () => {
      this.loading = this.loadingCtrl.create({
        spinner: 'crescent',
        content: 'Loading Please Wait...'
      });
      return this.loading;
    },
    dismiss_loader: () => {
      this.loading.dismiss();
    }
  };
}
